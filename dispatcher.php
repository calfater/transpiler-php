<?php

ini_set("error_reporting", E_ALL);
ini_set("display_errors", 1);

require __DIR__ . "/vendor/autoload.php";

function evalScript($matches) {
    return eval($matches[2] . ";");
}

$PAGES_DIR = __DIR__ . "/data/pages";
$TPL_PAGE_EXT = ".pug";

$STYLE_DIR = __DIR__ . "/data";
$STYLE_PAGE_EXT = ".less";

$PAGES_CACHE_DIR = "/tmp/transpiler";

$DEFAULT_PAGES_404= __DIR__ . "/default/_404.pug";

$url = $_GET["uri"];
$ext = preg_replace("#(^/)||(.*\\.)#", "", $url);
$pageNameWithoutExt = preg_replace("#(^/)||(\\..*)#", "", $url);

//var_dump($_GET);

if (empty($pageNameWithoutExt)) {
    $pageNameWithoutExt = "index";
    $ext = "html";
}

function getDirContents($dir, &$results = array()){
    $files = scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            $results[] = $path;
        } else if($value != "." && $value != "..") {
            getDirContents($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}

if ($ext == "css") {
    $resourcePath = "$STYLE_DIR/$pageNameWithoutExt$STYLE_PAGE_EXT";

    if (!file_exists($resourcePath)) {
        echo $resourcePath;
        header("HTTP/1.0 404 Not Found");
        return;
    }
    try {
        $parser = new Less_Parser();
        $parser->parseFile($resourcePath, '/');
        header("Content-type: text/css");
        echo $parser->getCss();
    } catch (Exception $e) {
        header("HTTP/1.0 500 " . str_replace("\n", "", $e->getMessage()));
        echo $e->getMessage();
        return;
    }
} else if ("html" == $ext) {

    $resourcePath = "$PAGES_DIR/$pageNameWithoutExt$TPL_PAGE_EXT";

    if (!file_exists($resourcePath)) {
        header("HTTP/1.0 404 Not Found");
        $resourcePath = "${PAGES_DIR}/404$TPL_PAGE_EXT";
        if (!file_exists($resourcePath)) {
            $resourcePath = "${DEFAULT_PAGES_404}";
        }
    }

    $pug = new \Pug\Pug(array(
        'pretty' => true,
        'cache' => $PAGES_CACHE_DIR
    ));
    $finalHtml = $pug->render($resourcePath, array(
        'page_name' => $pageNameWithoutExt
    ));

    $finalHtml = preg_replace_callback("/(<\\?php)(.*?)(\\?>)/s", "evalScript", $finalHtml);

    echo $finalHtml;

} else if ("/sitemap.xml" == $url) {
    header("Content-type: application/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    echo "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
    foreach (getDirContents(__DIR__ . "/../data/") as $file) {
        if (preg_match("/\.pug$/", $file)) {
            echo " <url>\n";
            echo "  <loc>$_SERVER[REQUEST_SCHEME]://$_SERVER[HTTP_HOST]". preg_replace("/^\/data/", "", preg_replace("/\.pug$/", "",$file)) . "</loc>\n";
            echo " </url>\n";
        }
    }
    echo "</urlset>\n";
} else {
    header("HTTP/1.0 404 Not Found");
    echo "$url not found";
}
