# _


This transpile :

 - **pug** files into **html** and **eval php** code
 - **less** into **css**


## pug

_pug_ files must be located in **pages** folder /data/**pages/a file.pug** and can be accessed with url : http://domain/a file.**html**

/!\ Note the missing **page/** in the url.

## less

_less_ files can be located anywhere /data/somewhere/**my style.less** and can be accessed with url : http://domain/somewhere/my style.**css**

## php

_php_ files /data/**a/script.php** can be accessed directly with url : http://domain/somewhere/**a/script.php**

## html

_html_ files /data/pages/file.html or /data/file.html can both be accessed directly with url : http://domain/**file.html**

## other resources

_other resources_ files /data/**a/resource.ext** can be accessed directly with url : http://domain/somewhere/**a/resource.ext**

# Run

docker run -d -p 3001:80 -v /tmp/data:/data --name transpiler-php calfater/transpiler-php